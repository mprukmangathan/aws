from boto3.dynamodb.conditions import Key
import boto3
import os
import datetime
import time

dynamodb = boto3.client('dynamodb')
dynamodb_table = os.environ["DYNAMODB_TABLE_NAME"]
dynamodbr = boto3.resource('dynamodb')
table = dynamodbr.Table(dynamodb_table)

def Put_Item(Mydict):
    try:
         response = dynamodb.put_item(TableName=dynamodb_table,Item=Mydict)
    except Exception as e:
         print('Error performing Put_Item')
         print(e)
         response = e
    return response

def Query(pkey):
    try:
         response = table.query(KeyConditionExpression=Key('accountid').eq(pkey))
    except Exception as e:
         print('Error performing Query')
         print(e)
         response = e
    return response

def QuerywithSortKey(pkey,skey):
    try:
         response = table.query(KeyConditionExpression=Key('accountid').eq(pkey) & Key('region').eq(skey))
    except Exception as e:
         print('Error performing Query')
         print(e)
         response = e
    return response
    
def QueryIndex(index_name,index,pkey):
    try:
         response = table.query(IndexName=index_name, KeyConditionExpression=Key(index).eq(pkey))
    except Exception as e:
         print('Error performing Query')
         print(e)
         response = e
    return response
    

def Update_Item(Mydict):
    updateexpression = 'SET ' + '#zz' + ' = :value, lastmodifieddate = :now'
    try:
         response = table.update_item(
                          Key={ 'accountid': Mydict['pkey'], 'region': Mydict['skey'] },
                          UpdateExpression=updateexpression,
                          ExpressionAttributeValues={ ':value': Mydict["value"], ':now': get_time() },
                          ExpressionAttributeNames={ '#zz': Mydict["key"] }
                    )
    except Exception as e:
         print('Error performing Update_Item')
         print(e)
         response = e
    return response

def UpdateExpressionItem(Mydict):
    updateexpression = 'SET ' + Mydict['expression'] + ' = :value, lastmodifieddate = :now'
    try:
         response = table.update_item(
                          Key={ 'accountid': Mydict['pkey'], 'region': Mydict['skey'] },
                          UpdateExpression=updateexpression,
                          ExpressionAttributeValues={ ':value': Mydict["value"], ':now': get_time() },
                          ExpressionAttributeNames={ Mydict['names']: Mydict["key"] }
                    )
    except Exception as e:
         print('Error performing Update_Item')
         print(e)
         response = e
    return response

def get_time():
    Today = datetime.date.today()
    Time = time.strftime("%H:%M:%S")
    Now = ("%sT%sZ" %(Today,Time))
    return(Now)
