import boto3
import json
import os
import datetime
import time
import ast
import DynamoDB
import Events
import LifeCycle

nintydayslater = datetime.datetime.today() + datetime.timedelta(days=90)
expiryDateTime = int(time.mktime(nintydayslater.timetuple()))

mandatory_tags = os.environ['MANDATORY_TAGS']
mtags = mandatory_tags.split(',')
aws_regions = os.environ['REGIONS']
Regions = aws_regions.split(',')
default_region = "us-east-1"

fHeaders = {
     'Account_Name'              : 'name',                   
     'Account_ID'                : 'accountid',              
     'Account_Status'            : 'status',                 
     'Account_Email'             : 'email',                  
     'JoinedTimestamp'           : 'joinedtimestamp',        
     'Parent_OU_Id'              : 'parent_ou_id',           
     'Parent_OU_Name'            : 'parent_ou_name',         
     'Provisioned_product_ID'    : 'provisioned_product_id', 
     'List_of_VPCs'              : 'list_of_vpcs',
     'CFN_INPUT_VPCRegion'       : 'cfn_input_vpc_region',   
     'CFN_INPUT_OrgUnitName'     : 'cfn_input_org_unitname', 
     'CFN_INPUT_VPCCidr'         : 'cfn_input_vpc_cidr',     
     'CFN_INPUT_PeerVPC'         : 'cfn_input_peervpc',      
     'CFN_INPUT_Email'           : 'cfn_input_email',        
     'CFN_INPUT_Account_Name'    : 'cfn_input_account_name', 
     'CFN_OUTPUT_Email'          : 'cfn_output_email',       
     'CFN_OUTPUT_Account_Name'   : 'cfn_output_account_name',
     'tags'                      : 'non_mandatory_tags'
}


def sqsevent(Data, Region):
    Subject = Data['Subject']
    Message = Data['Message']
    TopicArn = Data['TopicArn']
    TopicName = TopicArn.split(':')[-1]
    print("## 3 --> TopicName: %s" %TopicName)
    if TopicName == 'lz-tactical-inventory-Alerts':
       dynamodata = {}
       no_of_acc_in_db = []
       index_name = "region-Index"
       index = "region"
       for region in Regions:
           response = DynamoDB.QueryIndex(index_name,index,region)
           dynamodata[region] = response['Items']
           for accdb in response['Items']:
               if accdb['accountid'] not in no_of_acc_in_db:
                  no_of_acc_in_db.append(accdb['accountid'])
       Dailydata(Message,Region,dynamodata,no_of_acc_in_db)
    #elif TopicName == 'lz-account-creation-termination-notification-Alerts':
    #   Events.sqscwevent(Subject,Message,Region)
    #else:
    #   print('Invalid TopicName')

        
def Dailydata(Message,Region,dbcache,no_of_acc_in_db):
    no_of_acc_in_db.sort()
    acc_count = 0
    no_of_acc_in_event = []
    Mydict = {}
    Mydict['region'] = Region
    #print(Mydict)
    Mylist = Message.split('\\n')
    for aitem in Mylist:
        all_items = aitem.split('\n')
    Head = all_items.pop(0)
    #Head = Mylist.pop(0)
    Heads = Head.split(',')
    Headers = []
    for head_item in Heads:
        val = head_item.strip()
        Headers.append(val.replace(" ", "_"))
    for item in all_items:
    #for item in Mylist:
        elements = item.split(',')
        no_of_elements = len(elements)
        for count in range(len(Headers)):
            Key = Headers[count]
            try:
                 Mydict[Key] = elements[count]
            except:
                 Mydict[Key] = "null"
                 
        
        if Mydict['Account_ID'] != "null":
           #print(Mydict)
           if Mydict['Account_ID'] not in no_of_acc_in_event:
              no_of_acc_in_event.append(Mydict['Account_ID'])
           
           acc_count = acc_count + 1
           print("#[%s]# Account: %s, Status: %s" %(acc_count,Mydict['Account_ID'],Mydict['Account_Status']))
           
           if '#' in Mydict["Provisioned_product_ID"]:
              Mydict["tags"] = Mydict["Provisioned_product_ID"] 
              
           ppid = Mydict["Provisioned_product_ID"].split("-")[0]
           if ppid != "pp":
              Mydict["Provisioned_product_ID"] = "null"
              
           jts = Mydict["JoinedTimestamp"]
           Mydict["JoinedTimestamp"] = datetime.datetime.strptime(jts, '%m/%d/%Y %H:%M:%S').strftime('%Y-%m-%dT%H:%M:%SZ')
           
           Mydict["cloudops"] = "CTI GL CIS PUBLIC CLOUD"
           #Mydict["used_service"] = ["CloudTrail::Trail"]
           Mydict["lastmodifieddate"] = get_time()
           
           if Mydict["tags"] == "no_tag":
              for mtag in mtags:
                  Mydict[mtag] = "null"
              #nonmTags = get_tags()
              Mydict["tags"] = {}
           else:
              acctaglist = Mydict["tags"].split("#")
              nonmanlist = []
              for acctag in acctaglist:
                  mtaginsqs = []
                  aTaG = acctag.split("-",1)
                  key = aTaG.pop(0)
                  for vAl in aTaG:
                      value = vAl
                  if key in mtags:
                     Mydict[key] = value
                     mtaginsqs.append(key)
                  else:
                     if key != "null":
                        nonmandict = {}
                        nonmandict['tag_name'] = key
                        nonmandict['tag_value'] = value
                        nonmanlist.append(nonmandict)
              if nonmanlist:
                 Mydict["tags"] = nonmanlist
              else:
                 Mydict["tags"] = []
           Mydict["oupath"] = getoupath(Mydict["Parent_OU_Name"])
           if not Mydict["List_of_VPCs"]:
              Mydict["List_of_VPCs"] = []
           else:
              Mydict["List_of_VPCs"] = get_vpc(Mydict["List_of_VPCs"],Mydict['Account_ID'])
           print(Mydict)
           validate(Mydict,dbcache)
           
    no_of_acc_in_event.sort()
    print("Number of accounts in DB: %s" %(len(no_of_acc_in_db)))
    print("Number of accounts in Events: %s" %(len(no_of_acc_in_event)))

def validate(dbdict,dbcache):
    iDict = Format(dbdict)
    pkey = iDict['accountid']
    skey = iDict['region']
    Account_exist = "NO"
    for entry in dbcache[skey]:
        if entry['accountid'] == pkey:
           Account_exist = "YES"
           myaccount = entry
    #response = DynamoDB.QuerywithSortKey(pkey,skey)
    if Account_exist == "NO":
       print("## account %s not exist in DB" %pkey)
       print("## Recording lifecycle for an account - %s" %pkey)
       LifeCycle.recordevent(iDict)
       if iDict['status'] == 'SUSPENDED':
          iDict["ttl"] = str(expiryDateTime)
       if iDict['status'] == 'ACTIVE':
          for mtaG in mtags:
              if mtaG not in iDict.keys():
                 iDict[mtaG] = "null" 
       actdict = ReSt(iDict)
       time.sleep(4)
       print("## Insert Item: %s" %actdict)
       db_pi_response = DynamoDB.Put_Item(actdict)
       print(dbresponse(db_pi_response))
    elif (Account_exist == "YES" and myaccount['status'] != 'SUSPENDED'):
       print("## account %s already exist in DB" %pkey)
       for item_key in iDict.keys():
           #print("## Validating %s" %item_key)
    
           if item_key == "list_of_vpcs":
              if iDict[item_key] != "null":
                 dbvpc = myaccount[item_key]
                 sort_dbvpc = sorted(dbvpc, key = lambda i: i['vpc_ids'])
                 sqsvpc = iDict[item_key]
                 sort_sqsvpc = sorted(sqsvpc, key = lambda i: i['vpc_ids'])
                 if sort_dbvpc != sort_sqsvpc:
                    print("%s != %s, updating DB" %(sort_dbvpc,sort_sqsvpc))
                    resp = dbupdate(pkey,skey,item_key,sort_sqsvpc)
                    print(dbresponse(resp))
                 else:
                    print("VPC: [ %s == %s ]" %(myaccount[item_key],iDict[item_key]))
           elif (item_key == "lastmodifieddate" or item_key == "used_service" or item_key == "joinedtimestamp"):
              print('lastmodifieddate,used_service,joinedtimestamp Excluding from validation')
           elif item_key == "non_mandatory_tags":
              #print("## Validating non-mandatory-tags")
              dbtagS = myaccount[item_key]
              sqsdatatagS = iDict[item_key]
              sort_dbtagS = sorted(dbtagS, key = lambda i: i['tag_value'])
              sort_sqsdatatagS = sorted(sqsdatatagS, key = lambda i: i['tag_value'])
              if sort_dbtagS != sort_sqsdatatagS:
                 print("%s != %s, updating DB" %(sort_dbtagS,sort_sqsdatatagS))
                 resp = dbupdate(pkey,skey,item_key,sort_sqsdatatagS)
                 print(dbresponse(resp))
              else:
                 print("Non-Mandatory-Tags: [ %s == %s ]" %(myaccount[item_key],iDict[item_key])) 
           else:
              try:
                   if myaccount[item_key] != iDict[item_key]:
                      print("%s != %s, updating DB" %(myaccount[item_key],iDict[item_key]))
                      resp = dbupdate(pkey,skey,item_key,iDict[item_key])
                      print(dbresponse(resp))
              except Exception as e:
                   print(e)
                   resp = dbupdate(pkey,skey,item_key,iDict[item_key])
                   print(dbresponse(resp))

       for Mtag in mtags:
           if Mtag not in iDict.keys():
              if Mtag not in myaccount.keys():
                 print("## Mandatory tag %s is not present in DB & SQS data" %Mtag)
                 update_mtag = dbupdate(pkey,skey,Mtag,"null")
                 print(dbresponse(update_mtag))      
    else:
       print('Invalid entry at validation')

def ReSt(Mydict):
    for itEm in Mydict:
        if itEm == "used_service":
           Mydict[itEm] = Dict('L',Mydict[itEm])
        elif itEm == "ttl":
           Mydict[itEm] = Dict('N',Mydict[itEm]) 
        elif itEm == "list_of_vpcs":
           if Mydict[itEm]:
              parent = []  
              for avpc in Mydict[itEm]:
                  vpclist = avpc['vpc_ids']
                  tmp = {}
                  tmplist = []
                  for ai in vpclist:
                      tmplist.append(Dict('S',ai))
                  tmp['vpc_ids'] = Dict('L',tmplist)
                  tmp['region'] = Dict('S',avpc['region'])
                  parent.append(Dict('M',tmp))
              Mydict[itEm] = Dict('L',parent) 
           else:
              Mydict[itEm] = Dict('L',[])
        elif itEm == "non_mandatory_tags":
           if Mydict[itEm]:
              nonmanroot = []
              for nmt in Mydict[itEm]:
                  tmp = {}
                  tmp['tag_name'] = Dict('S',nmt['tag_name'])
                  tmp['tag_value'] = Dict('S',nmt['tag_value'])
                  nonmanroot.append(Dict('M',tmp))
              Mydict[itEm] = Dict('L',nonmanroot)
           else:
              Mydict[itEm] = Dict('L',[])
        else:
           Mydict[itEm] = Dict('S',Mydict[itEm])
    return Mydict


def Dict(Type,aItem):
    Temp = {}
    Temp[Type] = aItem
    return Temp


def Format(Mydict):
    newformatdict = {}
    for mydicthead in Mydict.keys():
        if mydicthead in fHeaders.keys():
            newformatdict[fHeaders[mydicthead]] = Mydict[mydicthead]
        else:
            newformatdict[mydicthead] = Mydict[mydicthead]
    return newformatdict

def get_vpc(vpcs, accid):
    Network = []
    vpc = vpcs.split('#')
    tmpdict={}
    for network in vpc:
        vpcid = network.split('-')[0] + '-' + network.split('-')[1]
        vpc_region = network.split('-')[2] + '-' + network.split('-')[3] + '-' + network.split('-')[4]
        if vpc_region not in tmpdict.keys():
           tmpdict[vpc_region] = []
        tmpdict[vpc_region].append(vpcid)
    for reGion in tmpdict.keys():
        tmp = {}
        tmp['region'] = reGion
        tmp['vpc_ids'] = tmpdict[reGion]
        Network.append(tmp)
    return Network
    
def getoupath(parentouName):
    tmp = parentouName.replace("_", "/")
    oupath = '/' + tmp
    return oupath
    

def get_time():
    Today = datetime.date.today()
    Time = time.strftime("%H:%M:%S")
    Now = ("%sT%sZ" %(Today,Time))
    return(Now)

def get_tags():
    account_tags = {
        "CMP_VPCregion": "null",
        "CMP_VPCCidr": "null",
        "CMP_PeerVPC": "null",
        "CMP_VPCOptions": "null",
        "CMP_regions": "null",
        "cart_req_soeid": "null"
    } 
    return account_tags
    
def dbupdate(pkey,skey,key,value):
    print("updating %s" %key)
    DBQuery = {}
    DBQuery["pkey"] = pkey
    DBQuery["skey"] = skey
    DBQuery["key"] = key
    DBQuery["value"] = value
    response = DynamoDB.Update_Item(DBQuery)
    return response

def dbupdateexp(pkey,skey,key,value,exp,name):
    print("updating %s" %key)
    DBdict = {}
    DBdict['pkey'] = pkey
    DBdict['skey'] = skey
    DBdict['key'] = key
    DBdict['value'] = value
    DBdict['expression'] = exp
    DBdict['names'] = name
    response = DynamoDB.UpdateExpressionItem(DBdict)
    return response

def dbresponse(response):
    outPut = ""
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
       outPut = "DB Update was success"
    else:
       outPut = "DB update was failed"
    return outPut
