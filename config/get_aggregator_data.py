import json
import boto3
import os
import sys
import datetime
import time
import gzip

client = boto3.client('config')
s3 = boto3.client('s3')
bucket_name = os.environ["S3_BUCKET_NAME"]
today = datetime.date.today()
YYYY = today.year
MM = today.month
DD = today.day
Full_Date = ("%s/%s/%s" %(YYYY,MM,DD))
hh_mm = time.strftime("%H-%M")
Fname = ("%s.json" %hh_mm)
Bucket_file_name = Fname + ".gz"
zipfile = '/tmp/' + Fname + ".gz"
directory_name = "Snapshot/" + Full_Date

def config_aggregator(event, context):
    Aggregator_name = os.environ["AGGREGATOR_NAME"]
    envvar = os.environ["RESOURCE_TYPES"]
    Resourcetypes = envvar.split(',')
    with gzip.open(zipfile, 'wt') as zipit:
        zipit.write('{ \"ResourceIdentifiers\" : [ ')
        for Resourcetype in Resourcetypes:
            ladr = client.list_aggregate_discovered_resources(
                      ConfigurationAggregatorName=Aggregator_name,
                      ResourceType=Resourcetype
                   )
            for resource in ladr["ResourceIdentifiers"]:
                Garc = client.get_aggregate_resource_config(
                          ConfigurationAggregatorName=Aggregator_name,
                          ResourceIdentifier=resource
                       )
                zipit.write(json.dumps(Garc, indent=4, sort_keys=True, default=str))
        zipit.write('] }'+'\n')
        zipit.close()
    s3.upload_file(zipfile, bucket_name, directory_name + "/" + Bucket_file_name)
