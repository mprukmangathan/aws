from boto3.dynamodb.conditions import Key
from datetime import date, timedelta
import json
import boto3
import os
import datetime
import time

STS = boto3.client('sts')
rolearn = os.environ["Config_Aggregator_Role"]
cred = STS.assume_role(
             RoleArn=rolearn,
             RoleSessionName="cross_acct_lambda"
         )
ACCESS_KEY = cred['Credentials']['AccessKeyId']
SECRET_KEY = cred['Credentials']['SecretAccessKey']
SESSION_TOKEN = cred['Credentials']['SessionToken']

tmp = date.today()
today = str(tmp)
tmp = tmp - timedelta(days = 1)
yesterday = str(tmp)

aws_regions = os.environ['REGIONS']
regions = aws_regions.split(',')
default_region = 'us-east-1'

Config = boto3.client('config',aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY, aws_session_token=SESSION_TOKEN)
dynamodb_table = os.environ["DYNAMODB_TABLE_NAME"]
aggregator_name = os.environ["CONFIG_AGGREGATOR_NAME"]
envvar = os.environ["RESOURCE_TYPES"]
Resourcetypes = envvar.split(',')
dynamodbr = boto3.resource('dynamodb')
table = dynamodbr.Table(dynamodb_table)
CostExplorer = boto3.client('ce',aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY, aws_session_token=SESSION_TOKEN)

ce_services = {}

ce_resource_type = {
    "AWS CloudTrail": "CloudTrail::Trail",
    "AWS Config": "Config::ResourceCompliance",
    "AWS Key Management Service": "KMS::Key",
    "AWS Lambda": "Lambda::Function",
    "Amazon DynamoDB": "DynamoDB::Table",
    "EC2 - Other": "EC2::Instance",
    "Amazon Elastic Compute Cloud - Compute": "EC2::Instance",
    "Amazon Simple Notification Service": "SNS::Topic",
    "Amazon Simple Queue Service": "SQS::Queue",
    "Amazon Simple Storage Service": "S3::Bucket",
    "AmazonCloudWatch": "CloudWatch::Alarm",
    "CloudWatch Events": "CloudWatch::Events",
    "Amazon Virtual Private Cloud": "EC2::VPC"
}

def lambda_handler(event, context):
    acc_count = 0
    acc_update = 0
    acc_not_update = 0
    in_active_acc = 0
    agg = get_used_services_from_config(Resourcetypes)
    print(agg)
    response = table.query(IndexName="region-Index",KeyConditionExpression=Key('region').eq(default_region))
    print(response)
    for accounts in (response['Items']):
        acc_count = acc_count + 1
        acc_id = accounts['accountid']
        print("#[%s]# Account: %s" %(acc_count,acc_id))
        aRegion = accounts['region']
        status = accounts['status']
        
        if status == 'SUSPENDED':
           print("#[%s]# Account %s is not active ####" %(acc_count,acc_id))
           in_active_acc = in_active_acc + 1
           continue

        ce_data = get_us_from_billing(acc_id)
 
        #compdata = agg_data + list(set(second_list) - set(first_list))
        aggdata = []
        
        for reG in regions:
            compdata = []
            try:
                agg_data = agg[acc_id][reG]
                agg_data.sort()
            except:
                agg_data = []
                
            try:
                coex_data = ce_data[acc_id][reG]
                coex_data.sort()
            except:
                coex_data = []
                
            compdata = agg_data + list(set(coex_data) - set(agg_data))
            compdata.sort()
            
            try:    
                tmp = {}
                tmp['region'] = reG
                tmp['services'] = compdata
                aggdata.append(tmp)
            except Exception as e:
                aggdata = aggdata
        try:
            if accounts['used_service']:
               us_from_db = accounts['used_service']

        except:
            us_from_db = []
           
        sort_us_from_db = sorted(us_from_db, key = lambda i: i['services'])
        print("Used Services from DB: %s" %sort_us_from_db)
        sort_aggdata = sorted(aggdata, key = lambda i: i['services'])
        print("Used Services from Aggregator: %s" %sort_aggdata)
        
        if sort_us_from_db == sort_aggdata:
           print("Used services are identical for account %s" %acc_id)
           acc_not_update = acc_not_update + 1
        else:
           print("Used services are NOT identical for account %s" %acc_id)
           acc_update = acc_update + 1
           usdict = {}   
           usdict['pkey'] = acc_id    
           usdict['skey'] = default_region   
           usdict['key']  = "used_service"
           usdict['value'] = sort_aggdata
           print(usdict)
           response = Update_Item(usdict)
           print(dbresponse(response))
           
    print("Total number of accounts                  : %s" %acc_count)
    print("Total number of suspended accounts        : %s" %in_active_acc)
    print("Total number of accounts are modified     : %s" %acc_update)
    print("Total number of accounts are not modified : %s" %acc_not_update)

def get_used_services_from_config(Resourcetypes):
    used_services = {}
    for Resourcetype in Resourcetypes:
        ladr = Config.list_aggregate_discovered_resources(
                      ConfigurationAggregatorName=aggregator_name,
                      ResourceType=Resourcetype
               )
        for resource in ladr["ResourceIdentifiers"]:
            acc_id = resource['SourceAccountId']
            rt = resource['ResourceType']
            region = resource['SourceRegion']
            rType = rt.split('::')[1]  + "::" + rt.split('::')[2]
            if acc_id not in used_services:
               used_services[acc_id] = {}
            if region not in used_services[acc_id]:
               used_services[acc_id][region] = []
            if acc_id in used_services.keys():
               if rType not in used_services[acc_id][region]:
                  used_services[acc_id][region].append(rType)
    return (used_services)
    
def get_us_from_billing(acc_id):
    if acc_id not in ce_services.keys():
       ce_services[acc_id] = {}
    billing_info = get_resource_from_billing(acc_id)
    #print("Billing Info: %s" %billing_info)
    if billing_info:
       for bytime in billing_info['ResultsByTime']:
           Groups = bytime['Groups']
           for keys in Groups:
               servinfo = keys['Keys']
               #print("Keys %s" %servinfo)
               if 'Tax' not in servinfo:
                  Reg = ''
                  for tmp in servinfo:
                      tmplist = tmp.split('-')
                      if len(tmplist) == 3:
                         Reg = tmp
                      elif len(tmplist) == 1:
                         item = tmp
                  if Reg in regions:
                     if Reg not in ce_services[acc_id].keys():
                        ce_services[acc_id][Reg] = []
                     try:
                        ce_keys = ce_resource_type[item]
                     except Exception as e:
                        #ce_keys = item
                        ce_keys = item.replace(" ","::")
                     if ce_keys not in ce_services[acc_id][Reg]:
                        ce_services[acc_id][Reg].append(ce_keys)
    return(ce_services)
    
def get_resource_from_billing(aws_account_id):
    try:
         response = CostExplorer.get_cost_and_usage(
                            TimePeriod={
                                          'Start': yesterday,
                                          'End': today
                                       },
                            Filter={
                                      'Dimensions': {
                                           'Key': 'LINKED_ACCOUNT',
                                           'Values': [ aws_account_id ],
                                      }
                            },
                            Granularity='MONTHLY',
                            Metrics=[ 'BlendedCost', 'UnblendedCost', 'UsageQuantity'],
                            GroupBy=[
                                {
                                   'Type': 'DIMENSION',
                                   'Key': 'REGION'
                                },
                                {
                                   'Type': 'DIMENSION',
                                   'Key': 'SERVICE'
                                }
                            ]
                    )
    except Exception as e:
         print("Error pulling billing info for account - %s" %aws_account_id)
         print(e)
         response = None
    return response

def Update_Item(Mydict):
    updateexpression = 'SET ' + '#zz' + ' = :value, lastmodifieddate = :now'
    try:
         response = table.update_item(
                          Key={ 'accountid': Mydict['pkey'], 'region': Mydict['skey'] },
                          UpdateExpression=updateexpression,
                          ExpressionAttributeValues={ ':value': Mydict["value"], ':now': get_time() },
                          ExpressionAttributeNames={ '#zz': Mydict["key"] }
                    )
    except Exception as e:
         print('Error performing Update_Item')
         response = e
    return response
    
def get_time():
    Today = datetime.date.today()
    Time = time.strftime("%H:%M:%S")
    Now = ("%sT%sZ" %(Today,Time))
    return(Now)

def dbresponse(response):
    outPut = ""
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
       outPut = "DB Update was success"
    else:
       outPut = "DB update was failed"
    return outPut
