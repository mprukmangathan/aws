#!/usr/bin/python3

import boto3
import json
import csv

Headers = [ "AccountID", "Region", "ResourceID", "Instance_Status", "ImageID"]

Config = boto3.client('config')
AggregatorName = "secops-config-aggregator-prod-ctiops"
Resourcetype = "AWS::EC2::Instance"

ladr = Config.list_aggregate_discovered_resources(
              ConfigurationAggregatorName=AggregatorName,
              ResourceType=Resourcetype
       )

ec2_resource = {}
unique = []
count=0

while True:
      for resource in ladr["ResourceIdentifiers"]:
          accountid = resource['SourceAccountId']
          if accountid not in unique:
             count = count + 1
             print("[%s] %s" %(count,accountid))
             unique.append(accountid)
          if accountid not in ec2_resource.keys():
             ec2_resource[accountid] = []
          ec2 = Config.get_aggregate_resource_config(ConfigurationAggregatorName=AggregatorName,ResourceIdentifier=resource)
          ec2_resource[accountid].append(ec2)

      if 'NextToken' in ladr:
         ladr = Config.list_aggregate_discovered_resources(ConfigurationAggregatorName=AggregatorName,ResourceType=Resourcetype,NextToken=ladr['NextToken'])
      else:
         break

with open('ec2_ami.csv', 'w', newline='') as csvfile:
     writer = csv.writer(csvfile)
     writer.writerow(Headers)
     for acc_id in ec2_resource.keys():
         print(acc_id)
         for data in ec2_resource[acc_id]:
             row = []
             resourceId = data['ConfigurationItem']['resourceId']
             awsRegion = data['ConfigurationItem']['awsRegion']
             Configuration = data['ConfigurationItem']['configuration']
             Config = json.loads(Configuration)
             Imageid = Config['imageId']
             Status = Config['state']['name']
             row = [ acc_id, awsRegion, resourceId, Status, Imageid]
             writer.writerow(row)
