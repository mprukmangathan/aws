"""Module responsible for defining the AWS Lambda handler."""
import time
from os import path
import os
from datetime import datetime
import boto3

#ACCOUNT_ID = os.environ.get("ACCOUNT_ID")
#SNS_TOPIC_ARN = os.environ.get("SNS_TOPIC_ARN")
ACCOUNT_ID = 971691587463 
SNS_TOPIC_ARN = "arn:aws:sns:us-east-1:971691587463:TacticalInventory"

def get_cfn_info_all_accounts():
    """function to return a list stacks that start by SC-<Master account No>-<provisioned product ID> and then add the provisioned product ID to the dict"""
    list_all_account = []
    sc_client = boto3.client("servicecatalog")
    cfn_client = boto3.client("cloudformation")
    paginator = sc_client.get_paginator("scan_provisioned_products")
    pages = paginator.paginate(AccessLevelFilter={"Key": "Account", "Value": "self"},)

    for page in pages:

        for account in page["ProvisionedProducts"]:
            if account["Status"] == "AVAILABLE":
                # print(account["Name"])

                provisioned_products = sc_client.search_provisioned_products(
                    AccessLevelFilter={"Key": "Account", "Value": "self"},
                    Filters={"SearchQuery": [account["Name"]]},
                )

                for product in provisioned_products["ProvisionedProducts"]:
                    if product["Name"] == account["Name"]:
                        # print("provisioned product is {} \n----".format(product["Id"]))
                        StackName = "SC-" + str(ACCOUNT_ID) + "-" + product["Id"]

                try:

                    stack_description = cfn_client.describe_stacks(StackName=StackName)
                    #print(StackName)

                    #print(provisioned_products["ProvisionedProducts"])
                    #print("\n")
                    if len(provisioned_products["ProvisionedProducts"]) > 0:
                        stack_description["ProvisionedProductID"] = provisioned_products["ProvisionedProducts"][0]["Id"]
                        list_all_account.append(stack_description)
                except:
                    print(
                        "Execption for this pp {}".format(
                            provisioned_products["ProvisionedProducts"]
                        )
                    )
                    pass
    # print(list_all_account)
    return list_all_account

def get_vpc_info_all_accounts():
    """function that returns all VPC in all accounts from config"""
    list_of_vpc= []
    config_client = boto3.client("config")
    paginator = config_client.get_paginator("list_aggregate_discovered_resources")
    pages = paginator.paginate(
        ConfigurationAggregatorName='Master_Account_Aggregator',
        ResourceType='AWS::EC2::VPC',
    )

    for page in pages:
        
        for vpc in page["ResourceIdentifiers"]:
            list_of_vpc.append(vpc)

    return(list_of_vpc)


def get_accounts(cfn_info,vpc_info):
    """Sends/Prints the list of ALL accounts from AWS organizations and adds parents info and stack info"""
    message = "Account Name,Account ID,Account Status,Account Email,JoinedTimestamp, Parent OU Id,Parent OU Name,List of VPCs,Provisioned product ID,\
        CFN_INPUT_VPCRegion,CFN_INPUT_OrgUnitName,CFN_INPUT_VPCCidr,CFN_INPUT_PeerVPC,CFN_INPUT_Email,CFN_INPUT_Account Name,\
            CFN_OUTPUT_Email,CFN_OUTPUT_Account Name,tags\n"
    time.sleep(0.050)
    dev_session = boto3.session.Session()
    client = dev_session.client("organizations")
    paginator = client.get_paginator("list_accounts")
    pages = paginator.paginate()

    for page in pages:
        # print(page)
        if len(page["Accounts"]) != 0:
            for acc in page["Accounts"]:
                # GET PARENT OU

                immediate_parent = client.list_parents(ChildId=acc["Id"])
                time.sleep(0.150)

                row = [
                    acc["Name"],
                    acc["Id"],
                    acc["Status"],
                    acc["Email"],
                    acc["JoinedTimestamp"].strftime("%m/%d/%Y"),
                ]

                ou_tree = ""
                if immediate_parent["Parents"][0]["Id"].startswith("ou", 0):
                    first_ou_description = client.describe_organizational_unit(
                        OrganizationalUnitId=immediate_parent["Parents"][0]["Id"]
                    )
                    ou_tree = first_ou_description["OrganizationalUnit"]["Name"]
                    ou_id = first_ou_description["OrganizationalUnit"]["Id"]
                    while True:
                        try:
                            #print(ou_id)
                            parent_of_parent = client.list_parents(ChildId=ou_id)
                            #print(parent_of_parent)
                            ou_id = parent_of_parent["Parents"][0]["Id"]
                            ou_description = client.describe_organizational_unit(
                                OrganizationalUnitId=parent_of_parent["Parents"][0]["Id"]
                            )
                            ou_tree = (
                                ou_description["OrganizationalUnit"]["Name"] + "_" + ou_tree
                            )
                        except Exception as e:
                            #print("exception {}".format(e))
                            break

                    add_elem = [
                        first_ou_description["OrganizationalUnit"]["Id"],
                        ou_tree,
                    ]

                row.extend(add_elem)

                vpc_for_acc = []
                for vpc in vpc_info:
                    if vpc['SourceAccountId'] == acc["Id"]:
                        vpc_for_acc.append(vpc['ResourceId']+"-"+vpc['SourceRegion'])        
                vpc_string = "#".join(vpc_for_acc)
                row.append(vpc_string)

                for item in cfn_info:
                    if item["Stacks"][0]["Outputs"][0]["OutputValue"] == acc["Id"]:
                        # print(item['ProvisionedProductID'])
                        row.append(item["ProvisionedProductID"])
                        for param in item["Stacks"][0]["Parameters"]:
                            if param["ParameterKey"] == "VPCRegion":
                                row.append(param["ParameterValue"])
                            if param["ParameterKey"] == "OrgUnitName":
                                row.append(param["ParameterValue"])
                            if param["ParameterKey"] == "VPCCidr":
                                row.append(param["ParameterValue"])
                            if param["ParameterKey"] == "PeerVPC":
                                row.append(param["ParameterValue"])
                            if param["ParameterKey"] == "AccountEmail":
                                row.append(param["ParameterValue"])
                            if param["ParameterKey"] == "AccountName":
                                row.append(param["ParameterValue"])

                        for param in item["Stacks"][0]["Outputs"]:
                            if param["OutputKey"] == "AccountEmail":
                                row.append(param["OutputValue"])
                            if param["OutputKey"] == "AccountName":
                                row.append(param["OutputValue"])

                account_tags = client.list_tags_for_resource(ResourceId=acc["Id"])
                Tags = account_tags["Tags"]
                if Tags:
                   tags_for_acc = []
                   for item in Tags:
                       tags_for_acc.append(item['Key']+"-"+item['Value'])
                   tag_string = "#".join(tags_for_acc)
                   row.append(tag_string)
                else:
                   tag_string = "no_tag"
                   row.append(tag_string)  

                #new_line = ",".join(row[0:17]) + "\n"
                new_line = ",".join(row) + "\n"
                if new_line[-1] != "\n":
                    print("New Line missing carriage return")
                    new_line = new_line + "\n"
                message = message + new_line
                #message = message + ",".join(row) + "\n"

    sns_client = boto3.client("sns")

    date = datetime.today().strftime("%Y-%m-%d")
    subject = str(ACCOUNT_ID) + " - AWS Accounts Tactical Inventory - " + date
    print(subject)
    print(message)
    #sns_client.publish(TopicArn=SNS_TOPIC_ARN, Message=message, Subject=subject)
    return


if __name__ == "__main__":

    vpc_info = get_vpc_info_all_accounts()
    cfn_info = get_cfn_info_all_accounts()

    get_accounts(cfn_info,vpc_info)
