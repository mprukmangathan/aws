#!/usr/bin/python

import boto3
import csv

def awsaccid(acc_id):
    tot_count = len(acc_id)
    if tot_count == 12:
       return acc_id
    else:
       missingdigit = 12 - tot_count
       j = ""
       for i in range(missingdigit):
           j = j + "0"
       accountid = j + str(acc_id)
       return accountid

Mydict = {}
with open('account_tags.csv') as csv_file:
     csv_reader = csv.reader(csv_file, delimiter=',')
     line_count = 0
     for row in csv_reader:
         if line_count == 0:
            line_count += 1
            Headers = row
         else:
            temp = {}
            for count in range(len(Headers)):
                tmp = Headers[count]
                Key = tmp.replace(" ", "_")
                try:
                    temp[Key] = row[count]
                except Exception as e:
                    temp[Key] = "null"

            accountid = awsaccid(temp['Account_ID'])
            temp['Account_ID'] = accountid
            if accountid not in Mydict:
               Mydict[accountid] = []
            Mydict[accountid].append(temp)

client = boto3.client('organizations')
all_accounts = client.list_accounts()
mandatory_tags = { "application_id"                  : "csiappid", 
                   "chargeback_profile_id"           : "billingprofileid", 
                   "secondary_chargeback_profile_id" : "secondarybillingprofileid", 
                   "account_type"                    : "account_type", 
                   "request__id"                     : "requestid", 
                   "app_inst_id"                     : "appinstid", 
                   "organization_unit"               : "organization_unit",
                   "cloud_support_group"             : "cloudops",
                   "architecture_review_id"          : "cartid" 
                 }

while True:
  for account_info in all_accounts['Accounts']:
      aws_acc_id = account_info['Id']
      if aws_acc_id not in Mydict:
         print("Account %s missing from speadsheet" %aws_acc_id)
         continue
      myaccount = Mydict[aws_acc_id][0]
      parents = client.list_parents(ChildId=aws_acc_id)
      myaccount["organization_unit"] = parents["Parents"][0]['Id'] 
      man_tags = []
      for man_tag_key in mandatory_tags.keys():
          tmp = {}
          tmp["Key"] = str(mandatory_tags[man_tag_key])
          tmp["Value"] = str(myaccount[man_tag_key])
          man_tags.append(tmp)
      print("Updating tags for account %s" %myaccount["Account_ID"])
      print("Mandatory Tags: %s" %man_tags)
      try:
          response = client.tag_resource(ResourceId=str(myaccount["Account_ID"]),Tags=man_tags)
          if response["ResponseMetadata"]["HTTPStatusCode"] == 200:
             print ("Successfully updated the db for account " + myaccount["Account_ID"])
          else:
             print ("failed to update the db for account " + myaccount["Account_ID"]) 
      except Exception as e:
          print("Error mapping tags for account: %s" %myaccount["Account_ID"])
          print("Error: %s" %e)
  if 'NextToken' in all_accounts:
     all_accounts = client.list_accounts(NextToken=all_accounts['NextToken'])
  else:
     break
