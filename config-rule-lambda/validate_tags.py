import json
import boto3

APPLICABLE_RESOURCES = ["AWS::EC2::Instance"]

missing_tags = []

def find_violation(current_tags, required_tags):
    violation = ""
    tag_present = False
    rtvalues = required_tags.values()
    #ckeys = current_tags.keys()
    #print (current_tags)
    #print (rtvalues)
    #print (ckeys)
    for spval in rtvalues:
        req_tags = spval.split(',')
        print (req_tags)
    for mtag in req_tags:
        print (mtag)
        if mtag in current_tags:
           tag_present = True
        else:
           missing_tags.append(mtag)
    if missing_tags:
       violation = violation + "\n Missing mandatory tags !"
    if not tag_present:
       violation = violation + "\n Tag is not present."
    if violation == "":
       return None
    return violation

def evaluate_compliance(configuration_item, rule_parameters):
    if configuration_item["resourceType"] not in APPLICABLE_RESOURCES:
        return {
            "compliance_type": "NOT_APPLICABLE",
            "annotation": "The rule doesn't apply to resources of type " +
            configuration_item["resourceType"] + "."
        }

    if configuration_item["configurationItemStatus"] == "ResourceDeleted":
        return {
            "compliance_type": "NOT_APPLICABLE",
            "annotation": "The configurationItem was deleted and therefore cannot be validated."
        }

    #current_tags = configuration_item["configuration"].get("tags")
    current_tags = configuration_item["tags"]
    print (current_tags)
    violation = find_violation(current_tags, rule_parameters)   
    print(violation)

    if violation:
        return {
            "compliance_type": "NON_COMPLIANT",
            "annotation": violation
        }

    return {
        "compliance_type": "COMPLIANT",
        "annotation": "This resource is compliant with the rule."
    }

def lambda_handler(event, context):
    #print (event)
    invoking_event = json.loads(event["invokingEvent"])
    configuration_item = invoking_event["configurationItem"]
    #print (configuration_item)
    rule_parameters = json.loads(event["ruleParameters"])

    result_token = "No token found."
    if "resultToken" in event:
        result_token = event["resultToken"]

    evaluation = evaluate_compliance(configuration_item, rule_parameters)

    config = boto3.client("config")
    config.put_evaluations(
        Evaluations=[
            {
                "ComplianceResourceType":
                    configuration_item["resourceType"],
                "ComplianceResourceId":
                    configuration_item["resourceId"],
                "ComplianceType":
                    evaluation["compliance_type"],
                "Annotation":
                    evaluation["annotation"],
                "OrderingTimestamp":
                    configuration_item["configurationItemCaptureTime"]
            },
        ],
        ResultToken=result_token
    )
