import boto3
import json
import os
import datetime
import time
import ast
import DynamoDB
import Events

def recordevent(Mydict):
    event = {}
    event["detail"] = {}
    event["detail"]["awsRegion"] = Mydict["region"]
    event["detail"]['serviceEventDetails'] = {}
    event['detail']['serviceEventDetails']['createAccountStatus'] = {}
    event['detail']['serviceEventDetails']['createAccountStatus']['accountId'] = Mydict["accountid"]
    event['detail']['serviceEventDetails']['createAccountStatus']['state'] = "SUCCEEDED"
    requestedTimestamp = datetime.datetime.strptime(Mydict["joinedtimestamp"], '%Y-%m-%dT%H:%M:%SZ').strftime('%b %d, %Y %H:%M:%S %p')
    event['detail']['serviceEventDetails']['createAccountStatus']['requestedTimestamp'] = requestedTimestamp
    tmp = datetime.datetime.strptime(requestedTimestamp, '%b %d, %Y %H:%M:%S %p') + datetime.timedelta(seconds=4)
    completedTimestamp = tmp.strftime('%b %d, %Y %H:%M:%S %p')
    event['detail']['serviceEventDetails']['createAccountStatus']['completedTimestamp'] = completedTimestamp
    Events.CreateAccount(event)
