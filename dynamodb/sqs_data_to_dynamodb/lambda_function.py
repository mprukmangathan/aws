import ast
import DataProc
import Events

def lambda_handler(event, context):
    print("## 1 --> Events: %s" %event)
    try:
        for Records in event['Records']:
            Body = Records['body']
            Region = Records['awsRegion']
            #stringdata = Records['body']
            #Body = ast.literal_eval(stringdata)
            print("## 2 --> Body: %s" %Body)
        DataProc.sqsevent(Body,Region)
    except Exception as e:
        #print(e)
        if 'errorCode' in event['detail']:
            print('## 2 --> Error events, Exiting')
            print("## 3 --> ErrorCode: %s" %event['detail']['errorCode'])
            exit(1)
        else:
            Events.cwevent(event)
