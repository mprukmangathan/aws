import boto3
import json
import os
import datetime
import time
import ast
import DynamoDB
import DataProc

mandatory_tags = os.environ['MANDATORY_TAGS']
mtags = mandatory_tags.split(',')

def sqscwevent(Subject,Message,Region):
    print("Subject => %s" %Subject)
    print("Message => %s" %message)
    print("Region  => %s" %Region)

def cwevent(event):
    #print(event)
    eventname = event['detail']['eventName']
    print("## 2 --> EventName: %s" %eventname)
    if eventname == 'MoveAccount':
       response = MoveAccount(event)
    elif eventname == 'TagResource':
       response = TagResource(event)
    elif eventname == 'CreateAccountResult':
       response = CreateAccount(event)

def CreateAccount(event):
    accountid = event['detail']['serviceEventDetails']['createAccountStatus']['accountId']
    region = event['detail']['awsRegion']
    state = event['detail']['serviceEventDetails']['createAccountStatus']['state']
    if state == 'SUCCEEDED':
       created_time = event['detail']['serviceEventDetails']['createAccountStatus']['requestedTimestamp']
       updated_time = event['detail']['serviceEventDetails']['createAccountStatus']['completedTimestamp']
       created_time_lmd = datetime.datetime.strptime(created_time, '%b %d, %Y %H:%M:%S %p').strftime('%Y-%m-%dT%H:%M:%SZ')
       updated_time_lmd = datetime.datetime.strptime(updated_time, '%b %d, %Y %H:%M:%S %p').strftime('%Y-%m-%dT%H:%M:%SZ')
       Joinedtimestamp = datetime.datetime.strptime(created_time, '%b %d, %Y %H:%M:%S %p').strftime('%Y-%m-%dT%H:%M:%SZ')
       Mydict = {}
       Mydict['accountid'] = DataProc.Dict('S',accountid)
       Mydict['region'] = DataProc.Dict('S',region)
       Mydict['joinedtimestamp'] = DataProc.Dict('S',Joinedtimestamp)
       Mydict['lastmodifieddate'] = DataProc.Dict('S',created_time_lmd)
       Mydict['status'] = DataProc.Dict('S','IN_PROGRESS')
       response = DynamoDB.Put_Item(Mydict)
       print(DataProc.dbresponse(response))
       time.sleep(4)
       Mydict['lastmodifieddate'] = DataProc.Dict('S',updated_time_lmd)
       Mydict['status'] = DataProc.Dict('S',state)
       response = DynamoDB.Put_Item(Mydict)
       print(DataProc.dbresponse(response))
       time.sleep(4)
       active_fmt = datetime.datetime.strptime(updated_time_lmd, '%Y-%m-%dT%H:%M:%SZ') + datetime.timedelta(seconds=4)
       active_time_lmd = active_fmt.strftime('%Y-%m-%dT%H:%M:%SZ')
       Mydict['lastmodifieddate'] = DataProc.Dict('S',active_time_lmd)
       Mydict['status'] = DataProc.Dict('S','ACTIVE')
       response = DynamoDB.Put_Item(Mydict)
       print(DataProc.dbresponse(response))
       
def MoveAccount(event):
    madict = {}
    accountid = event['detail']['requestParameters']['accountId']
    region = event['detail']['awsRegion']
    source = event['detail']['requestParameters']['sourceParentId']
    dest = event['detail']['requestParameters']['destinationParentId']
    madict['pkey'] = accountid
    madict['skey'] = region
    madict['key'] = 'parent_ou_id'
    madict['value'] = dest
    print(madict)
    response = DynamoDB.Update_Item(madict)
    print(DataProc.dbresponse(response))
    #response = "success"
    #print(response)
    UpdateOUPath(madict)
    return (response)
    
def UpdateOUPath(oudict):
    index_name = 'ParentOUId-Index'
    index = oudict['key']
    response = DynamoDB.QueryIndex(index_name,index,oudict['value'])
    if response['Count'] == 1:
       print('New ou id')
    else:
       for item in response['Items']:
           if item['accountid'] != oudict['pkey']:
              oudict['key'] = 'parent_ou_name'
              oudict['value'] = item['parent_ou_name']
              print(oudict)
              ounameresp = DynamoDB.Update_Item(oudict)
              print(DataProc.dbresponse(ounameresp))
              oudict['key'] = 'oupath'
              oudict['value'] = item['oupath']
              print(oudict)
              oupathresp = DynamoDB.Update_Item(oudict)
              print(DataProc.dbresponse(oupathresp))
              break

def TagResource(event):
    madict = {}
    accountid = event['detail']['requestParameters']['resourceId']
    region = event['detail']['awsRegion']
    tags = event['detail']['requestParameters']['tags']
    key = tags[0]['key']
    value = tags[0]['value']
    print("Events --> Key: %s, Value: %s" %(key,value))
    query_response = DynamoDB.QuerywithSortKey(accountid,region)
    if query_response['Count'] == 1:
       if key in mtags:
          madict['pkey'] = accountid
          madict['skey'] = region
          madict['key'] = key
          madict['value'] = value
          print("DB --> Key: %s, Value: %s" %(key,value))
          if query_response['Items'][0][key] == value:
             print("value are equal: %s == %s" %(query_response['Items'][0][key],value))
          else:
             print("value are not equal: %s != %s" %(query_response['Items'][0][key],value))
             update_response = DynamoDB.Update_Item(madict)
             print(DataProc.dbresponse(update_response))
       else:
          dTags = query_response['Items'][0]['non_mandatory_tags']
          print(dTags)
          keylist = []

          if dTags:
             for aitem in dTags:
                 keylist.append(aitem['tag_name'])
                 if key == aitem['tag_name']:
                    if value != aitem['tag_value']:
                       aitem['tag_value'] = value

             if key not in keylist:
                tmp = {}
                tmp['tag_name'] = key
                tmp['tag_value'] = value
                dTags.append(tmp)
          else:
             tmp = {}
             tmp['tag_name'] = key
             tmp['tag_value'] = value
             dTags.append(tmp)
          
          print(dTags)
          DBdict = {}
          DBdict['pkey'] = accountid
          DBdict['skey'] = region
          DBdict['key'] = 'non_mandatory_tags'
          DBdict['value'] = dTags
          response = DynamoDB.Update_Item(DBdict)
          print(DataProc.dbresponse(response))
