#!/usr/bin/python

import json
import boto3
import datetime
import time

dynamodb = boto3.client('dynamodb')
dynamodb_table = "accounts-db-stream"
print('Loading function')

def get_time():
    Today = datetime.date.today()
    Time = time.strftime("%H:%M:%S")
    Now = ("%s %s" %(Today,Time))
    return(Now)

def lambda_handler(event, context):
    print('------------------------')
    print(event)
    try:
        for record in event['Records']:
            if record['eventName'] == 'INSERT':
                handle_insert(record)
            elif record['eventName'] == 'MODIFY':
                handle_modify(record)
            elif record['eventName'] == 'REMOVE':
                handle_remove(record)
        print('------------------------')
        return "Success!"
    except Exception as e: 
        print(e)
        print('------------------------')
        return "Error"


def handle_insert(record):
    print("Handling INSERT Event")
    Insert = {}
    newImage = record['dynamodb']['NewImage']
    newAccount = newImage['accountid']['S']
    newStatus = newImage['status']['S'] 
    print ('New account= ' + newAccount + ' with status= ' + newStatus)
    Insert["accountid"] = newImage['accountid']['S']
    Insert["date"] = get_time()
    Insert["status"] = newImage['status']['S']
    Insert["name"] = newImage['name']['S']
    Insert["usedservices"] = newImage['usedservices']['L']
    update_stream_table(Insert)
    print("Done handling INSERT Event")

def handle_modify(record):
    print("Handling MODIFY Event")
    oldImage = record['dynamodb']['OldImage']
    oldStatus = oldImage['status']['S']
    newImage = record['dynamodb']['NewImage']
    newStatus = newImage['status']['S']
    print('Account status changed from oldStatus= ' + oldStatus + ' to newStatus= ' + newStatus)
    Update = {}
    Update["accountid"] = newImage['accountid']['S']
    Update["date"] = get_time()
    Update["status"] = newImage['status']['S']
    Update["name"] = newImage['name']['S']
    Update["usedservices"] = newImage['usedservices']['L']
    update_stream_table(Update)
    print("Done handling MODIFY Event")

def handle_remove(record):
    print("Handling REMOVE Event")
    oldImage = record['dynamodb']['OldImage']
    oldAccount = oldImage['accountid']['S']
    oldStatus = oldImage['status']['S']
    print ('Account= ' + oldAccount + ' with status= ' + oldStatus + ' was removed from dynamodb')
    Delete = {}
    Delete["accountid"] = oldImage['accountid']['S']
    Delete["date"] = get_time()
    Delete["status"] = oldImage['status']['S']
    Delete["name"] = oldImage['name']['S']
    Delete["usedservices"] = oldImage['usedservices']['L']
    update_stream_table(Delete)
    print("Done handling REMOVE Event")
    
def update_stream_table(data):
    Update_Table = dynamodb.put_item(
        TableName=dynamodb_table,
        Item={
            "accountid"     : { "S": data["accountid"]      },
            "date"          : { "S": data["date"]           },
            "status"        : { "S": data["status"]         },
            "name"          : { "S": data["name"]           },
            "usedservices"  : { "L": data["usedservices"]   }
        }
    )
    return (Update_Table)
