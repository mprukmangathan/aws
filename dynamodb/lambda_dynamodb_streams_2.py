from boto3.dynamodb.conditions import Key
import boto3
import os
import datetime
import time

dynamodb = boto3.client('dynamodb')
dynamodb_table = os.environ["DYNAMODB_TABLE_NAME"]

dynamodb_resource = boto3.resource('dynamodb')
table = dynamodb_resource.Table(dynamodb_table)

ayearafter = datetime.datetime.today() + datetime.timedelta(days=365)
expiryDateTime = int(time.mktime(ayearafter.timetuple()))

def dbresponse(response):
    outPut = ""
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
       outPut = "DB Update was success"
    else:
       outPut = "DB update was failed"
    return outPut
    
def get_time():
    Today = datetime.date.today()
    Time = time.strftime("%H:%M:%S")
    Now = ("%sT%sZ" %(Today,Time))
    return(Now)

def Dict(Type,aItem):
    Temp = {}
    Temp[Type] = aItem
    return Temp
    
def Put_Item(Mydict):
    try:
         response = dynamodb.put_item(TableName=dynamodb_table,Item=Mydict)
    except Exception as e:
         print('Error performing Put_Item')
         print(e)
         response = e
    return response

def lambda_handler(event, context):
    print('------------------------')
    print(event)
    try:
        for record in event['Records']:
            if record['eventName'] == 'INSERT':
                handle_insert(record)
            elif record['eventName'] == 'MODIFY':
                handle_modify(record)
            elif record['eventName'] == 'REMOVE':
                handle_remove(record)
        print('------------------------')
        return "Success!"
    except Exception as e: 
        print(e)
        print('------------------------')
        return "Error"


def handle_insert(record):
    print("Handling INSERT Event")
    newImage = record['dynamodb']['NewImage']
    #newImage["lastmodifieddate"] = Dict('S',get_time())
    newImage["comments"] = Dict('S',"Insert Event")
    print(newImage)
    response = Put_Item(newImage)
    print(dbresponse(response))

def handle_modify(record):
    print("Handling MODIFY Event")
    newImage = record['dynamodb']['NewImage']
    oldImage = record['dynamodb']['OldImage']
    if (oldImage['status']['S'] == 'ACTIVE' or oldImage['status']['S'] == 'SUSPENDED'):
       newImage["lastmodifieddate"] = Dict('S',get_time())
    if oldImage['status']['S'] == 'SUSPENDED':
       oldImage.pop("ttl")
    if newImage['status']['S'] == 'SUSPENDED':
       newImage.pop("ttl")
    newImage["comments"] = Dict('S',"Modify Event")
    
    print(newImage)
    response = Put_Item(newImage)
    print(dbresponse(response))

def handle_remove(record):
    print("Handling REMOVE Event")
    oldImage = record['dynamodb']['OldImage']
    if oldImage['status']['S'] == 'SUSPENDED':
       oldImage['status'] = Dict('S','TERMINATED')
       oldImage.pop("ttl")
    else:
       oldImage['status'] = Dict('S','TERMINATED') 
    oldImage["lastmodifieddate"] = Dict('S',get_time())
    oldImage["comments"] = Dict('S',"Remove Event")
    oldImage["timetoexist"] = { 'N': str(expiryDateTime) }
    print(oldImage)
    response = Put_Item(oldImage)
    print(dbresponse(response))
    acc_id = oldImage['accountid']['S']
    try:
        response = table.query(KeyConditionExpression=Key('accountid').eq(acc_id))
        print(response)
        for item in response['Items']:
            try:
                  response = table.update_item(
                             Key={ 'accountid': item['accountid'], 'lastmodifieddate': item["lastmodifieddate"] },
                             UpdateExpression='SET #zz = :value',
                             ExpressionAttributeValues={ ':value': expiryDateTime },
                             ExpressionAttributeNames={ '#zz': 'timetoexist' }
                       )
                  print(dbresponse(response))
            except Exception as e:
                  print('Error performing Update_Item')
                  print(e)
    except Exception as e:
        print('Error performing Query')
        print(e)
