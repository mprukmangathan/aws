
Need two dynamodb's
1) accounts (stream option must be enabled)
2) accounts-db-stream

Lambda function role should have below aws managed policies
1) AmazonDynamoDBFullAccess (for dynamodb write access)
2) AWSLambdaDynamoDBExecutionRole

update dynamodb to trigger the lambda function
dynamodb item to be inserted into accounts table
{
  "accountid": "12334576",
  "date": "2020-08-06 05:36:21",
  "status": "created",
  "name": "development",
  "usedservices": ["EC2", "S3"]
}
