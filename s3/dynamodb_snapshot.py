from boto3.dynamodb.conditions import Key
import datetime
import zipfile
import boto3
import json
import time
import gzip
import zlib
import sys
import os

os.chdir('/tmp')

aws_regions = os.environ["REGIONS"]
regions = aws_regions.split(",")

compression = zipfile.ZIP_DEFLATED
mydate = datetime.datetime.now()
today = datetime.date.today()
YYYY = today.year
MM = mydate.strftime("%b")
DD = today.day
Full_Date = ("%s/%s/%s" %(YYYY,MM,DD))

Fname = "AWS-Accounts-daily-snapshot.json"
Zfname = "AWS-Accounts-daily-snapshot.zip"

directory_name = "dynamodb-cmdb-accounts-organisation-attributes-table-snapshot/" + Full_Date
Object = directory_name + "/" + Zfname

dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
s3 = boto3.client('s3', region_name='us-east-1')

dynamodb_table = os.environ["DYNAMODB_TABLE_NAME"]
s3_bucket_name = os.environ["S3_BUCKET_NAME"]
table = dynamodb.Table(dynamodb_table)

def lambda_handler(event, context):
    Data = {}
    print("## Querying Data from table: %s" %dynamodb_table)
    for region in regions:
        resp = table.query(
                     IndexName="region-Index",
                     KeyConditionExpression=Key('region').eq(region)
               )
        Data[region] = {}
        Data[region] = resp['Items']
    print("## Done")
    print("## Writing the DB data to file %s" %Fname)
    with open(Fname, 'w', encoding='utf-8') as f:
         f.write('{ \"Accounts\" : ')
         json.dump(Data, f, ensure_ascii=False, indent=4)
         f.write('}' + '\n')
         f.close()
    print("## Done")
    print("## Compressing the file %s" %Zfname)
    zf = zipfile.ZipFile(Zfname, mode='w')
    zf.write(Fname, compress_type=compression)
    zf.close()
    print("## Done")
    try:
       s3.upload_file(Zfname, s3_bucket_name, Object)
       print("## File %s uploaded to s3 bucket %s Successfully" %(Zfname,s3_bucket_name))
    except Exception as e:
       print('## Error uploading the file %s to S3 bucket %s' %(Zfname,s3_bucket_name))
       print(e)
